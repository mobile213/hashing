/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.hashing;

/**
 *
 * @author Lenovo
 */
public class Hashing<Key, Value> {

    private int M = 100;
    private Value[] vals = (Value[]) new Object[M];
    private Key[] keys = (Key[]) new Object[M];

    private int hash(Key key) {
        return (key.hashCode() & 0X7FFFFFFF) % M;
    }

    public void put(Key key, Value val) {
        int i;
        for (i = hash(key); keys[i] != null; i = (i + 1) % M)
        {
            if (keys[i].equals(key))
            {
                break;
            }
        }
        keys[i] = key;
        vals[i] = val;
    }

    public Value get(Key key) {
        for (int i = hash(key); keys[i] != null; i = (i + 1) % M)
        {
            if (key.equals(keys[i]))
            {
                return vals[i];
            }
        }
        return null;
    }

    public int remove(Key key) {
        int i = hash(key);
        while (keys[i] != null)
        {
            if (key.equals(keys[i]))
            {
                int temp = (int) keys[i];
                keys[i] = null;
                return temp;
            }
            i = (i + 1) % M;
        }
        return 0;
    }

}
